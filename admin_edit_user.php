<?php
    $user_id = $_GET['user_id'];
    $con = mysqli_connect('localhost', 'root', '@,~rXtn3hU5?3sn~', 'task_3');
    mysqli_set_charset($connect,"utf8");
    $select = mysqli_query($con, "select * from users where id = '$user_id'");
    $assoc = mysqli_fetch_assoc($select);
    $error = '';
    $explode_old_email = explode('@', $assoc['email']);
    $cv_array = explode(',', $assoc['cv']);
    if(isset($_POST['btn_save'])){
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $email = $_POST['email'];
        $bday = $_POST['bday'];
        $gender = $_POST['optradio'];
        $cv = $_FILES['cv'];
        $cv_name = $_FILES['cv']['name'];
        if($firstname != '' && $lastname != '' && $email != '' && $bday != '' && $gender != ''){
            $select = mysqli_query($con, "select * from users where email='$email' and id != '$user_id'");
            if(mysqli_num_rows($select) == 0){
                $explode_new_email = explode('@', $email);
                if($explode_old_email != $explode_new_email){
                    rename('upload/'.$explode_old_email[0],'upload/'.$explode_new_email[0]);
                }
                if($cv_name != '') {
                    $formats_array = ['pdf', 'doc', 'docx'];
                    for($i=0; $i<count($cv_name); $i++){
                        $explode_array = explode('.', $cv_name[$i]);
                        $allowed_format = end($explode_array);
                        if(!in_array($allowed_format, $formats_array)){
                            $check++;
                        }
                    }
                    if ($check == 0) {
                        $explode_email = explode('@', $email);
                        if (!is_dir('upload/' . $explode_email[0])) {
                            mkdir('upload/' . $explode_email[0], 0777);
                        }
                        if (!is_dir('upload/' . $explode_email[0] . '/cv')) {
                            mkdir('upload/' . $explode_email[0] . '/cv', 0777);
                        }

                        for($j=0; $j<count($cv_name); $j++){
                            $cv_tmp_name = $cv['tmp_name'][$j];
                            $dir = 'upload/' . $explode_email[0] . '/cv/' . $cv_name[$j];
                            $upload = move_uploaded_file($cv_tmp_name, $dir);
                            $cv_array[] = $cv_name[$j];
                        }
                    } else {
                        $error = 'Incorect format file.';
                    }
                }
                $cv = implode(',', $cv_array);
                $update = mysqli_query($con, "update users set firstname='$firstname', lastname='$lastname', email='$email', bday='$bday', gender='$gender', cv='$cv' where id='$user_id'");
                if($update){
                    header('Location:home.php');
                }else{
                    $error = 'Same problem with server.';
                }
            }else{
                $error = 'Please change your email address.';
                $email = '';
            }
        }else{
            $error = 'Please fill all filds.';
        }












    }
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>task_3</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <form class="form-horizontal" role="form" action="" method="post" enctype="multipart/form-data">
        <h2>Edit user profile</a></h2>
        <div class="form-group">
            <label for="firstName" class="col-sm-3 control-label">First Name</label>
            <div class="col-sm-9">
                <input type="text" id="firstName" placeholder="First Name" class="form-control <?php if(isset($_POST['btn_save']) && $firstname==''){echo 'red_border';} ?>" value="<?php if(isset($_POST['btn_save']) && $firstname==''){echo '';}else{echo $assoc['firstname'];} ?>" name="firstname">
            </div>
        </div>
        <div class="form-group">
            <label for="lastName" class="col-sm-3 control-label">Last Name</label>
            <div class="col-sm-9">
                <input type="text" id="lastName" placeholder="Last Name" class="form-control <?php if(isset($_POST['btn_save']) && $lastname==''){echo 'red_border';} ?>" value="<?php if(isset($_POST['btn_save']) && $lastname==''){echo '';}else{echo $assoc['lastname'];} ?>" name="lastname" >
            </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-sm-3 control-label">Email</label>
            <div class="col-sm-9">
                <input type="email" id="email" placeholder="Email" class="form-control  <?php if(isset($_POST['btn_save']) && $email==''){echo 'red_border';} echo $red_border; ?>" value="<?php if(isset($_POST['btn_save']) && $email==''){echo '';}else{echo $assoc['email'];} ?>" name="email">
            </div>
        </div>
        <div class="form-group">
            <label for="birthDate" class="col-sm-3 control-label">Date of Birth</label>
            <div class="col-sm-9">
                <input type="date" id="birthDate" class="form-control  <?php if(isset($_POST['btn_save']) && $bday==''){echo 'red_border';} ?>" value="<?php if(isset($_POST['btn_save']) && $bday==''){echo '';}else{echo $assoc['bday'];} ?>" name="bday">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3">Gender</label>
            <div class="col-sm-6">
                <label class="radio-inline"><input type="radio" name="optradio" value="male" <?php if($assoc['gender']=='male'){echo 'checked';} ?>>Male</label>
                <label style="margin-left:5px" class="radio-inline"><input type="radio" name="optradio" value="female" <?php if($assoc['gender']=='female'){echo 'checked';} ?>>Female</label>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3">Files</label> <br>
<!--            <iframe src="upload/vardansargsyan2/cv/Vazgen-Sargsyan-CV.pdf" width="100px" height="100px"></iframe>-->
<!--            <embed src="upload/vardansargsyan2/cv/Vazgen-Sargsyan-CV.docx" width="100px" height="100px" type="application/word">-->
<!--            <iframe src = "upload/vardansargsyan2/cv/Vazgen-Sargsyan-CV.docx" width='100px' height='100px'></iframe>-->
<!--            <embed src="upload/vardansargsyan2/cv/Vazgen-Sargsyan-CV.docx" width="100px" height="100px" type="application/msword">-->
<!--            <input type="file" src="upload/vardansargsyan2/cv/Vazgen-Sargsyan-CV.pdf" alt="Submit">-->
<!--            header('Content-type: application/vnd.ms-word');-->
<!--            header('Content-Disposition: attachment; filename="Vazgen-Sargsyan-CV.docx"');-->
<!--            readfile('upload/vardansargsyan2/cv/');-->
            <?php
                if($assoc['cv'] != ''){
                    for($i=0; $i<count($cv_array); $i++){
                        echo '<div class="cv"><a href="upload/'.$explode_old_email[0].'/cv/'.$cv_array[$i].'" download>'.$cv_array[$i].'</a><span class="cls"  value="'.$i.'" data-id="'.$user_id.'"></span></div>';
                    }
                }
            ?>
            <div class="col-sm-9" style="padding-left:0">
                    <span class="btn btn-default btn-file">
                         <input type="file" class="<?php if(isset($_POST['btn_reg']) && $_FILES['cv']['name']==''){echo 'red_border';} ?>" name="cv[]" multiple>
                    </span>
            </div>
        </div>
        <p class="error"><?= $error ?></p>
        <button type="submit" class="btn btn-primary btn-block" name="btn_save">Save</button>
    </form>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src = "js/jquery.js"></script>
<script src = "js/main.js"></script>
</body>
</html>
