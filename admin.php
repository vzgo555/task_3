<?php
    session_start();
    $con = mysqli_connect('localhost', 'root', '@,~rXtn3hU5?3sn~', 'task_3');
    mysqli_set_charset($connect,"utf8");
    $select = mysqli_query($con, "select * from users where role = '0'");
    for($i = 0; $i < $select->num_rows; $i++){
        $assoc[] = mysqli_fetch_assoc($select);
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>task_3</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
    <div class="log_out"><a href="logout.php">Log Out</a></div>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Id</th>
            <th scope="col">Firstname</th>
            <th scope="col">Lastname</th>
            <th scope="col">Email</th>
            <th scope="col">Password</th>
            <th scope="col">Birthday</th>
            <th scope="col">Gender</th>
            <th scope="col">CV</th>
            <th scope="col">Role</th>
            <th scope="col">#</th>
            <th scope="col">#</th>
        </tr>
        </thead>
        <tbody>
            <?php
                for($j = 0; $j < count($assoc); $j++){
                    echo '
                        <tr>
                            <td>'.$j.'</td>
                            <td>'.$assoc[$j]['id'].'</td>
                            <td>'.$assoc[$j]['firstname'].'</td>
                            <td>'.$assoc[$j]['lastname'].'</td>
                            <td>'.$assoc[$j]['email'].'</td>
                            <td>'.$assoc[$j]['password'].'</td>
                            <td>'.$assoc[$j]['bday'].'</td>
                            <td>'.$assoc[$j]['gender'].'</td>
                            <td>'.$assoc[$j]['cv'].'</td>
                            <td>'.$assoc[$j]['role'].'</td>
                            <td><a href = "admin_edit_user.php?user_id='.$assoc[$j]['id'].'"><button type="button" class="btn btn-warning edit_user" value="'.$assoc[$j]['id'].'">Edit</button></a></td>
                            <td><button type="button" class="btn btn-danger delete_user" value="'.$assoc[$j]['id'].'">Delete</button></td>
                        </tr>
                ';
                }
            ?>
        </tbody>
    </table>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src = "js/jquery.js"></script>
    <script src = "js/main.js"></script>
</body>
</html>
