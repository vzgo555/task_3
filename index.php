<?php
    $con = mysqli_connect('localhost', 'root', '@,~rXtn3hU5?3sn~', 'task_3');
    mysqli_set_charset($connect,"utf8");
    $error = '';
    $registr = false;
    $check = 0;
    if(isset($_POST['btn_reg'])){
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $c_password = $_POST['c_password'];
        $bday = $_POST['bday'];
        $gender = $_POST['optradio'];
        $cv = $_FILES['cv'];
        if($firstname != '' && $lastname != '' && $email != '' && $password != '' && $c_password != '' && $bday != '' && $gender != ''){
            $select = mysqli_query($con, "select * from users where email='$email'");
            if(mysqli_num_rows($select) == 0){
                if($password == $c_password){
                    if(strlen($password) >= 6){
                        $formats_array = ['pdf', 'doc', 'docx'];
                        $cv_name = $_FILES['cv']['name'];
                        if($cv_name != '') {
                            for($i=0; $i<count($cv_name); $i++){
                                $explode_array = explode('.', $cv_name[$i]);
                                $allowed_format = end($explode_array);
                                if(!in_array($allowed_format, $formats_array)){
                                    $check++;
                                }
                            }
                            if ($check == 0) {
                                $explode_email = explode('@', $email);
                                if (!is_dir('upload/' . $explode_email[0])) {
                                    mkdir('upload/' . $explode_email[0], 0777);
                                }
                                if (!is_dir('upload/' . $explode_email[0] . '/cv')) {
                                    mkdir('upload/' . $explode_email[0] . '/cv', 0777);
                                }

                                for($j=0; $j<count($cv_name); $j++){
                                    $cv_tmp_name = $cv['tmp_name'][$j];
                                    $dir = 'upload/' . $explode_email[0] . '/cv/' . $cv_name[$j];
                                    $upload = move_uploaded_file($cv_tmp_name, $dir);
                                }

                                if ($upload) {
                                    $cv_name = implode(',', $cv['name']);
                                    $password = md5($password);
                                    $insert = mysqli_query($con, "insert into users (firstname, lastname, email, password, bday, gender, cv, role) values ('$firstname', '$lastname', '$email', '$password', '$bday', '$gender', '$cv_name', '0')");
                                    $registr = true;
                                    $select = mysqli_query($con, "select * from users where email = '$email'");
                                    session_start();
                                    $_SESSION["user"] = mysqli_fetch_assoc($select);
                                    header('Location: home.php');
                                }
                            } else {
                                $error = 'Incorect format file.';
                            }
                        }else{
                            $error = 'Please choose file.';
                        }
                    }else{
                        $error = 'Password length is short, min 6 synvol.';
                    }
                }else{
                    $error = 'Password do not have confirm.';
                }
            }else{
                $error = 'Please change your email address.';
                $red_border = 'red_border';
                $email = '';
            }
        }else{
            $error = 'Please fill all filds.';
        }
    }
?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>task_3</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <form class="form-horizontal" role="form" action="" method="post" enctype="multipart/form-data">
            <h2>Registr or <a href="login.php">SignIn</a></h2>
            <div class="form-group">
                <label for="firstName" class="col-sm-3 control-label">First Name</label>
                <div class="col-sm-9">
                    <input type="text" id="firstName" placeholder="First Name" class="form-control <?php if(isset($_POST['btn_reg']) && $_POST['firstname']==''){echo 'red_border';} ?>" value="<?php if(isset($_POST['btn_reg']) && $registr==false){echo $_POST['firstname'];} ?>" name="firstname">
                </div>
            </div>
            <div class="form-group">
                <label for="lastName" class="col-sm-3 control-label">Last Name</label>
                <div class="col-sm-9">
                    <input type="text" id="lastName" placeholder="Last Name" class="form-control <?php if(isset($_POST['btn_reg']) && $_POST['lastname']==''){echo 'red_border';} ?>" value="<?php if(isset($_POST['btn_reg']) && $registr==false){echo $_POST['lastname'];} ?>" name="lastname" >
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <input type="email" id="email" placeholder="Email" class="form-control <?php if(isset($_POST['btn_reg']) && $_POST['email']==''){echo 'red_border';} echo $red_border;?>" value="<?php if(isset($_POST['btn_reg']) && $registr==false){echo $email;} ?>" name="email">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-3 control-label">Password</label>
                <div class="col-sm-9">
                    <input type="password" id="password" placeholder="Password" class="form-control <?php if(isset($_POST['btn_reg']) && $_POST['password']==''){echo 'red_border';} ?>" name="password">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-3 control-label">Confirm Password</label>
                <div class="col-sm-9">
                    <input type="password" id="password" placeholder="Password" class="form-control <?php if(isset($_POST['btn_reg']) && $_POST['c_password']==''){echo 'red_border';} ?>" name="c_password">
                </div>
            </div>
            <div class="form-group">
                <label for="birthDate" class="col-sm-3 control-label">Date of Birth</label>
                <div class="col-sm-9">
                    <input type="date" id="birthDate" class="form-control <?php if(isset($_POST['btn_reg']) && $_POST['bday']==''){echo 'red_border';} ?>" value="<?php if(isset($_POST['btn_reg']) && $registr==false){echo $_POST['bday'];} ?>" name="bday">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Gender</label>
                <div class="col-sm-6">
                    <label class="radio-inline"><input type="radio" name="optradio" value="male" checked>Male</label>
                    <label style="margin-left:5px" class="radio-inline"><input type="radio" name="optradio" value="female">Female</label>
                </div>
            </div>
            <div class="form-group">
                <label for="birthDate" class="col-sm-3 control-label">Add your files</label>
                <div class="col-sm-9" style="padding-left:0">
                    <span class="btn btn-default btn-file">
                         <input type="file" class="<?php if(isset($_POST['btn_reg']) && $_FILES['cv']['name']==''){echo 'red_border';} ?>" name="cv[]" multiple>
                    </span>
                </div>
            </div>
            <p class="error"><?php echo $error ?></p>
            <button type="submit" class="btn btn-primary btn-block" name="btn_reg">Register</button>
        </form>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src = "js/jquery.js"></script>
    <script src = "js/main.js"></script>
</body>
</html>